package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Proyectil {
	private double x;
	private double y;
	private double velocidad;
	private Image misil;
	
	public Proyectil(double x, double y) {
		this.x = x;
		this.y = y;
		velocidad = 4;
		misil = Herramientas.cargarImagen("misil2.png");
	}
	
	public void dibujar(Entorno e) {
		if(misil != null) {
			e.dibujarImagen(misil, x, y, 0, 0.1);
		}
	}

	public double getVelocidad() {
		return velocidad;
	}

	public void avanzar() {
		y -= velocidad;
	}
	
	public boolean collision() {
		if (y < -50) {
			return true;
		}
		return false;
	}
	
	public void destruir() {
		misil = null;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
