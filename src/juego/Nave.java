package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Nave {
	private double x;
	private double y;
	private double velocidad;
	private Image naveImg;
	
	public Nave(double x, double y) {
		this.x = x;
		this.y = y;
		velocidad = 5;
		naveImg = Herramientas.cargarImagen("Nave.png");
	}
	
	public void dibujar(Entorno e) {
		e.dibujarImagen(naveImg, x, y, 0, 0.07);
	}

	
	public void moverIzq() {
		x -=  velocidad;
	}
	public void moverDer() {
		x += velocidad;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	

}
