package juego;

import java.awt.Color;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego {
	public boolean colision(double x1, double y1, double x2, double y2, double d) {
		return (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2) < d*d;
	}
	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;
	private Nave nave;
	private Image fondo;
	private double yFondo;
	private Proyectil [] misil;
	private int i;
	private Enemigo [] enemigos;
	
	
	public Juego() {
		// Inicializa el objeto entorno
		this.entorno = new Entorno(this, "Prueba del Entorno", 800, 600);
		this.nave = new Nave(entorno.ancho() / 2,entorno.alto()-55);
		this.fondo = Herramientas.cargarImagen("fondo1.png");
		yFondo = 0;
		i = 0;
		
		misil = new Proyectil [10];
		//enemigo = new Enemigo(400,0);
		enemigos = new Enemigo [4];
		for (int j = 0; j < enemigos.length; j++) {
			enemigos[j] = new Enemigo(Math.random() * entorno.ancho(), 0);
		}

		// Inicia el juego!
		this.entorno.iniciar();
	

	}

	/**
	 * Durante el juego, el método tick() será ejecutado en cada instante y 
	 * por lo tanto es el método más importante de esta clase. Aquí se debe 
	 * actualizar el estado interno del juego para simular el paso del tiempo 
	 * (ver el enunciado del TP para mayor detalle).
	 */
	public void tick() {
		// Procesamiento de un instante de tiempo
		// ...
		

		
		//Muevo la nave
		if(entorno.estaPresionada('a')) {
			if(nave.getX() > 0) {
				nave.moverIzq();
			}	
		}
		if(entorno.estaPresionada('d')) {
			if(nave.getX() < entorno.ancho()) {
				nave.moverDer();
			}   
			
		}//
		
		//fondo ciclado
		entorno.dibujarImagen(fondo, entorno.ancho() / 2 , (entorno.alto() / 2) + yFondo , 0,1);
		entorno.dibujarImagen(fondo, entorno.ancho() / 2 , (-entorno.alto() / 2) + yFondo , 0,1);
		yFondo++;
	
		if (yFondo > 600) {
			yFondo = 0;
		}//
		
		//Crea los misiles segun la posicion de la nave
		if (i < misil.length && entorno.sePresiono(entorno.TECLA_ESPACIO) && misil[i] == null) {
			misil[i] = new Proyectil(nave.getX(), nave.getY());
		}//		
	
		//Dibujo la nave
		nave.dibujar(entorno);	 
		
		//Aumenta el i para ir crear nuevos misiles
		if (i < misil.length && entorno.sePresiono(entorno.TECLA_ESPACIO)){
			i++;
		}//
		
		//Dibuja a los enemigos
		for(int j = 0; j < enemigos.length; j++) {
			
			if(enemigos[j] !=null) {
				enemigos[j].dibujarenemi(entorno);
				enemigos[j].avanzar();
	
				if(!enemigos[j].estaFuera(entorno)) {
					enemigos[j] = null;
				}
			}else {
				enemigos[j] = new Enemigo(Math.random() * entorno.ancho(), -100); 
			}
		}//
	
		//Dibuja los misiles ya lanzados y verifica si choco con el borde 
		for(int p = 0; p < misil.length; p++) {
			if (misil[p] != null ) {
				misil[p].dibujar(entorno);
				misil[p].avanzar();
				
				if(misil[p].getY() < -50 ) {
					misil[p] = null;
	
				}
			}
		}//
		
		//Verifica si el misil colisiono con el enemigo
		for(int j = 0; j < enemigos.length; j++) {
			for(int p = 0; p < misil.length; p++) {
				if(misil[p] != null && enemigos[j]!= null && colision(misil[p].getX(), misil[p].getY(), enemigos[j].getX(), enemigos[j].getY(), 30)) {
					enemigos[j] = null;
					misil[p] = null;
				}
			}
				
		}
		
		
		
		
		entorno.cambiarFont("Arial", 18, Color.CYAN);
		entorno.escribirTexto("Cantidad de balas: " +misil.length+"/"+disparados(misil), 500, 100);

		
		
	}
	public int disparados(Proyectil [] a) {
		int cont =  0 ;
		for (int i = 0; i < a.length; i++) {
			if (a[i] == null) {
				cont ++;
			}
		}
		return cont;
	}
	
	
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Juego juego = new Juego();
		
		
		
		
	}

}
