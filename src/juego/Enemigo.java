package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Enemigo {
	
	private double x;
	private double y;
	private Image enemi;
	private Image bossenemi;
	
	public Enemigo(double x, double y) {
		this.x = x;
		this.y = y;
		enemi = Herramientas.cargarImagen("enemi.png");
		bossenemi = Herramientas.cargarImagen("bossenemi.png");
	}
		
	public void dibujarenemi(Entorno e) {
		if(enemi != null) {
			e.dibujarImagen(enemi, x, y, 0, 0.3);
		}
		
	}

	public boolean recibioImpac(Proyectil a) {
		return  ((x-a.getX())*(x-a.getX()) + (y-a.getY())*(y-a.getY()) < 50*50);
	}

	public void avanzar() {
		y+=3;
	
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}
	
	public boolean estaFuera(Entorno e) {
		if(this.x < 0 || this.x > e.ancho() + 200 || this.y < -200 || this.y > e.alto() + 200) {
			return false;
		}
		
		return true;
	}
	
	
	
	
}
